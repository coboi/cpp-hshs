#include <iostream>
#include <fstream>
#include <string>

std::ofstream cmd;
std::string name;
std::string password;
int selectedCmd;

void selectCmd(){
	cmd.open("execute.bat", std::ios::trunc);
	if (selectedCmd == 1){
		cmd << "netsh wlan start hostednetwork";
	} else if (selectedCmd == 2){
		cmd << "netsh wlan stop hostednetwork";
	} else if (selectedCmd == 3){
		cmd << "netsh wlan stop hostednetwork\n";
		cmd << "netsh wlan set hostednetwork mode=allow ssid=" << name << " key=" << password;
	} else if (selectedCmd == 4){
		cmd << "netsh wlan show hostednetwork";
	}
	cmd.close();

	system("execute.bat");
}

int main(){
	std::cout << "Hotspot setup" << std::endl << std::endl;
	std::cout << "1. Hidupkan hotspot" << std::endl;
	std::cout << "2. Matikan hotspot" << std::endl;
	std::cout << "3. Atur hotspot" << std::endl;
	std::cout << "4. Informasi hotspot" << std::endl;
	std::cout << "5. Keluar" << std::endl << std::endl;
	
	while(true) {
	    std::cout << "Pilih 1/2/3/4/5: ";
		std::cin >> selectedCmd;
		
		if (selectedCmd == 1){
			selectCmd();
		} else if (selectedCmd == 2){
			selectCmd();
		} else if (selectedCmd == 3){
			std::cout << "Nama hotspot: ";
			std::cin >> name;
			std::cout << "Password: ";
			std::cin >> password;

			selectCmd();
		} else if (selectedCmd == 4){
			selectCmd();
		} else if (selectedCmd == 5){
			break;
		} else{
			std::cout << "Perintah salah" << std::endl;
		}
	}

	return 0;
}